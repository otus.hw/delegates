﻿using System;
using System.Collections.Generic;
using System.Text;

namespace delegates
{
    public class Employee
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public float Salary { get; set; }

        public List<Employee> GetDefaultEmployees()
        {
            List<Employee> employees = new List<Employee>(){
                new Employee { Name = "Keanu", LastName = "Reeves", Salary = 1000  },
                new Employee { Name = "Owen", LastName = "Wilson", Salary = 5000 },
                new Employee { Name = "James", LastName = "Spader", Salary = 2000 },
                new Employee { Name = "Sandra", LastName = "Bullock", Salary = 500 },
            };
            return employees;
        }
    }
}
