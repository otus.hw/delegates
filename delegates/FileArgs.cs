﻿using System;
using System.Collections.Generic;
using System.Text;

namespace delegates
{
    public class FileArgs: EventArgs
    {
        public string FileName { get; private set; }

        public FileArgs(string filename)
        {
            FileName = filename;
        }
    }
}
