﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace delegates
{
    public class FileListener
    {
        public string Path { get; private set; }

        public event EventHandler<FileArgs> FileFound;
        public event EventHandler CancelListen;
        private CancelEventArgs CancelEventArgs;
        

        public FileListener(string path)
        {
            Path = path;
        }

        public void Listen()
        {
            IEnumerable<string> files = Directory.EnumerateFiles(Path);
            foreach (var file in files)
            {
                if (CancelEventArgs != null)
                    break;
                FileArgs args = new FileArgs(file);
                OnFileFound(args);

            }

        }

        protected virtual void OnFileFound(FileArgs e)
        {
            FileFound?.Invoke(this, e);
        }

        public void OnCancel(CancelEventArgs args)
        {
            CancelEventArgs = args;
        }


    }
}
