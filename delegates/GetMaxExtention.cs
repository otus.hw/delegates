﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace delegates
{
    public static class GetMaxExtention
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            if (e == null)
                return null;

            var max = e.First();
            foreach (var item in e)
            {
                if (getParametr(item) > getParametr(max))
                    max = item;
            }
            
            return max;
        }
    }
}
