﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            var employeeMAX = new Employee().GetDefaultEmployees().GetMax(x => x.Salary);

            Console.WriteLine($"Max item {employeeMAX.Name} salary {employeeMAX.Salary}");

            FileListener fileListener = new FileListener(@"c:\Windows\");
            fileListener.FileFound += Filefound;
            fileListener.Listen();

            Console.WriteLine("End");
        }

        static void Filefound(object sender, FileArgs e)
        {
            Console.WriteLine($"File found: {e.FileName}");
            Console.WriteLine($"Продолжть: Enter; Отмена: c");
            if (Console.ReadKey(true).KeyChar == 'c')
            {
                
                Cancel(sender );
            }
        }

        static void Cancel(object sender)
        {
            
            var f = (FileListener)sender;
            CancelEventArgs args = new CancelEventArgs();
            f.OnCancel(args);
            Console.WriteLine("Операция отменена");
        }
    }
}
